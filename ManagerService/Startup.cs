﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ManagerService.Startup))]
namespace ManagerService
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
