﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace EmployeeWebserviceREST
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "EmployeeNew" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select EmployeeNew.svc or EmployeeNew.svc.cs at the Solution Explorer and start debugging.
    public class EmployeeNew : IEmployeeNew
    {
        EmployeeDATADataContext data = new EmployeeDATADataContext();

        public bool AddEmployee(employee eml)
        {
            try
            {
                data.employees.InsertOnSubmit(eml);
                data.SubmitChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool DeleteEmployee(int idE)
        {
            try
            {
                employee employeeToDelete =
                        (from employee in data.employees where employee.empID == idE select employee).Single();
                data.employees.DeleteOnSubmit(employeeToDelete);
                data.SubmitChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public List<employee> GetProductList()
        {
            try
            {
                return (from employee in data.employees select employee).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public bool UpdateEmployee(employee eml)
        {
            try
            {
                employee employeeToModify =
                    (from employee in data.employees where employee.empID == eml.empID select employee).Single();
                employeeToModify.Age = eml.Age;
                employeeToModify.firstName = eml.firstName;
                employeeToModify.lastName = eml.lastName;
                employeeToModify.address = eml.address;
                data.SubmitChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
    }
}

